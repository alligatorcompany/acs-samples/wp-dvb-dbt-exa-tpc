
with 
nation as (
  select  
    {{ dbt.concat(['N_NATIONKEY']) }} as nation_bk,
    {{ dbt_utils.generate_surrogate_key(['N_NATIONKEY']) }} as nation_h
  from {{ ref('nation') }} 
),
supplier as (
  select  distinct
    {{ dbt.concat(['S_NATIONKEY']) }} as nation_bk,
    {{ dbt_utils.generate_surrogate_key(['S_NATIONKEY']) }} as nation_h
  from {{ ref('supplier') }} 
),
customer as (
  select  distinct
    {{ dbt.concat(['C_NATIONKEY']) }} as nation_bk,
    {{ dbt_utils.generate_surrogate_key(['C_NATIONKEY']) }} as nation_h
  from {{ ref('customer') }} 
)

select * from nation
 union 
select * from supplier
 union 
select * from customer

