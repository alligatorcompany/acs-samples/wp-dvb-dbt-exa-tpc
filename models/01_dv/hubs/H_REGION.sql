
with 
nation as (
  select  distinct
    {{ dbt.concat(['N_REGIONKEY']) }} as region_bk,
    {{ dbt_utils.generate_surrogate_key(['N_REGIONKEY']) }} as region_h
  from {{ ref('nation') }} 
),
region as (
  select  
    {{ dbt.concat(['R_REGIONKEY']) }} as region_bk,
    {{ dbt_utils.generate_surrogate_key(['R_REGIONKEY']) }} as region_h
  from {{ ref('region') }} 
)

select * from nation
 union 
select * from region

