
with 
orders as (
  select  
    {{ dbt_utils.generate_surrogate_key(['O_ORDERKEY'])}} as order_h,
    {{ dbt_utils.generate_surrogate_key(['O_CUSTKEY', 'O_ORDERSTATUS', 'O_TOTALPRICE', 'O_ORDERDATE', 'O_ORDERPRIORITY', 'O_CLERK', 'O_SHIPPRIORITY', 'O_COMMENT'])}} as hkdiff,
    O_ORDERKEY,
    O_CUSTKEY,
    O_ORDERSTATUS,
    O_TOTALPRICE,
    O_ORDERDATE,
    O_ORDERPRIORITY,
    O_CLERK,
    O_SHIPPRIORITY,
    O_COMMENT
  from {{ ref('orders') }} 
)
select * from orders
