
with 
customer as (
  select  
    {{ dbt_utils.generate_surrogate_key(['C_CUSTKEY'])}} as customer_h,
    {{ dbt_utils.generate_surrogate_key(['C_NAME', 'C_ADDRESS', 'C_NATIONKEY', 'C_PHONE', 'C_ACCTBAL', 'C_MKTSEGMENT', 'C_COMMENT'])}} as hkdiff,
    C_CUSTKEY,
    C_NAME,
    C_ADDRESS,
    C_NATIONKEY,
    C_PHONE,
    C_ACCTBAL,
    C_MKTSEGMENT,
    C_COMMENT
  from {{ ref('customer') }} 
)
select * from customer
