
with 
nation as (
  select  
    {{ dbt_utils.generate_surrogate_key(['N_NATIONKEY'])}} as nation_h,
    {{ dbt_utils.generate_surrogate_key(['N_NAME', 'N_REGIONKEY', 'N_COMMENT'])}} as hkdiff,
    N_NATIONKEY,
    N_NAME,
    N_REGIONKEY,
    N_COMMENT
  from {{ ref('nation') }} 
)
select * from nation
