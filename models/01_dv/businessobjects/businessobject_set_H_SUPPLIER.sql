
with 
query_00 as (
  select * from {{ ref('H_SUPPLIER') }} 
),
query_01 as (
  select * from {{ ref('S_SUPPLIER_S_EXA_STAGE_R_SUPPLIER_U_DBT') }} 
)

select

  query_00.SUPPLIER_BK as 'Business Key for Hub Supplier',

  query_01.S_SUPPKEY as 'S_SUPPKEY',
  query_01.S_NAME as 'S_NAME',
  query_01.S_ADDRESS as 'S_ADDRESS',
  query_01.S_NATIONKEY as 'S_NATIONKEY',
  query_01.S_PHONE as 'S_PHONE',
  query_01.S_ACCTBAL as 'S_ACCTBAL',
  query_01.S_COMMENT as 'S_COMMENT'
from 
   query_00 
  
  join  query_01  on query_01.SUPPLIER_H = query_00.SUPPLIER_H
  
