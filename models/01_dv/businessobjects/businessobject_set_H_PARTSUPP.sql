
with 
query_00 as (
  select * from {{ ref('H_PARTSUPP') }} 
),
query_01 as (
  select * from {{ ref('S_PARTSUPP_S_EXA_STAGE_R_PARTSUPP_U_DBT') }} 
)

select

  query_00.PARTSUPP_BK as 'Business Key for Hub Partsupp',

  query_01.PS_PARTKEY as 'PS_PARTKEY',
  query_01.PS_SUPPKEY as 'PS_SUPPKEY',
  query_01.PS_AVAILQTY as 'PS_AVAILQTY',
  query_01.PS_SUPPLYCOST as 'PS_SUPPLYCOST',
  query_01.PS_COMMENT as 'PS_COMMENT'
from 
   query_00 
  
  join  query_01  on query_01.PARTSUPP_H = query_00.PARTSUPP_H
  
