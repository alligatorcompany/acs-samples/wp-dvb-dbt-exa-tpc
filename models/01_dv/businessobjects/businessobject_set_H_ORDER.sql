
with 
query_00 as (
  select * from {{ ref('H_ORDER') }} 
),
query_01 as (
  select * from {{ ref('S_ORDER_S_EXA_STAGE_R_ORDERS_U_DBT') }} 
)

select

  query_00.ORDER_BK as 'Business Key for Hub Order',

  query_01.O_ORDERKEY as 'O_ORDERKEY',
  query_01.O_CUSTKEY as 'O_CUSTKEY',
  query_01.O_ORDERSTATUS as 'O_ORDERSTATUS',
  query_01.O_TOTALPRICE as 'O_TOTALPRICE',
  query_01.O_ORDERDATE as 'O_ORDERDATE',
  query_01.O_ORDERPRIORITY as 'O_ORDERPRIORITY',
  query_01.O_CLERK as 'O_CLERK',
  query_01.O_SHIPPRIORITY as 'O_SHIPPRIORITY',
  query_01.O_COMMENT as 'O_COMMENT'
from 
   query_00 
  
  join  query_01  on query_01.ORDER_H = query_00.ORDER_H
  
