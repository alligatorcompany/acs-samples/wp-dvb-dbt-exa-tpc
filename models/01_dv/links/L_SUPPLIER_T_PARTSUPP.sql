
with 
partsupp as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['PS_SUPPKEY','PS_PARTKEY','PS_SUPPKEY']) }} as supplier_t_partsupp_h,
      
    {{ dbt_utils.generate_surrogate_key(['PS_SUPPKEY']) }} as supplier_h,
    PS_SUPPKEY as supplier_PS_SUPPKEY,
      
    {{ dbt_utils.generate_surrogate_key(['PS_PARTKEY','PS_SUPPKEY']) }} as partsupp_h,
    PS_PARTKEY as partsupp_PS_PARTKEY,
    PS_SUPPKEY as partsupp_PS_SUPPKEY
      
    from {{ ref('partsupp') }} 
)

select * from partsupp
