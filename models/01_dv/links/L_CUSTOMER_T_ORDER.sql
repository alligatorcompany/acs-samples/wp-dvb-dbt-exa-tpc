
with 
orders as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['O_CUSTKEY','O_ORDERKEY']) }} as customer_t_order_h,
      
    {{ dbt_utils.generate_surrogate_key(['O_CUSTKEY']) }} as customer_h,
    O_CUSTKEY as customer_O_CUSTKEY,
      
    {{ dbt_utils.generate_surrogate_key(['O_ORDERKEY']) }} as order_h,
    O_ORDERKEY as order_O_ORDERKEY
      
    from {{ ref('orders') }} 
)

select * from orders
