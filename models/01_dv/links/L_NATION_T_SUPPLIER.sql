
with 
supplier as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['S_NATIONKEY','S_SUPPKEY']) }} as nation_t_supplier_h,
      
    {{ dbt_utils.generate_surrogate_key(['S_NATIONKEY']) }} as nation_h,
    S_NATIONKEY as nation_S_NATIONKEY,
      
    {{ dbt_utils.generate_surrogate_key(['S_SUPPKEY']) }} as supplier_h,
    S_SUPPKEY as supplier_S_SUPPKEY
      
    from {{ ref('supplier') }} 
)

select * from supplier
