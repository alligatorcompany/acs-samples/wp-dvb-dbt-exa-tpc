
with 
customer as (
  select  distinct
    {{ dbt_utils.generate_surrogate_key(['C_NATIONKEY','C_CUSTKEY']) }} as nation_t_customer_h,
      
    {{ dbt_utils.generate_surrogate_key(['C_NATIONKEY']) }} as nation_h,
    C_NATIONKEY as nation_C_NATIONKEY,
      
    {{ dbt_utils.generate_surrogate_key(['C_CUSTKEY']) }} as customer_h,
    C_CUSTKEY as customer_C_CUSTKEY
      
    from {{ ref('customer') }} 
)

select * from customer
